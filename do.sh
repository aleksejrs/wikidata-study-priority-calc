#!/usr/bin/env bash

# Source files names are like "Q123@description.wdk".  ".wdk" is used for a
# file manager to start a text editor with several windows open:
# known/filename, unknown/filename, unneeded/filename
# The files contain lines like "Q123|Name from Wikidata".

declare -r EASEANDUSE_BITABLE_HEIGHTS=16
#declare -r EASEANDUSE_BITABLE_HEIGHTS=100

function normalize() {
	# This takes too long for its little use.

	# Take the _top use_ or _top use possible_, and multiply all numbers so that the given top or top possible one is 100.
	declare -i TOPNUM=$1
	declare -i TOPNUMCOEFF=$((10000 / TOPNUM))
	declare -i TEHNUM
	declare -i NORMNUM
	# Note that the qline is read without any whitespace at the beginning.
	while read qline ; do
		TEHNUM=$(echo $qline|grep -o "^[0-9]\+ ")
		NORMNUM=$((TEHNUM * TOPNUMCOEFF / 100))
		echo -n $NORMNUM
		echo $qline|sed "s/^[0-9]\+//"
	done < $2
}

function put_together() {
	NUMBERS_FROM="$1"
	while read q ; do

		paste --delimiters="|" -- <(grep --color=never --max-count=1 " $q$" "work/$NUMBERS_FROM") <(grep --color=never --max-count=1 -- "$q|" work-save/все_нормальные_строки_из_known_и_unknown_уникально|cut --delimiter="|" --fields=2- --)

	done < work/то,_что_знаю_из_чего-то_и_нужно_для_чего-то_ещё,_но_само_по_себе_не_изучал |grep --color=never -v " Q620946|" --|sort -rn --
}

function put_uses_together() {
	#NUMBERS_FROM=work/все_q_из_unknown_counts_normalized
	NUMBERS_FROM=work/все_q_из_unknown_counts

	while read q ; do
               paste --delimiters="|" -- <(grep --color=never --max-count=1 " $q$" "$NUMBERS_FROM") <(grep --color=never --max-count=1 -- "^$q|" work-save/все_нормальные_строки_из_known_и_unknown_уникально|cut --delimiter="|" --fields=2- --)

	done<work/все_q_из_unknown

}


function use_of_file() {
	FILE_TO_FIND_USE_OF=$1
	declare -i TOTAL_LINES
	declare -i USESUM
	grep --only-matching "^Q[0-9]\+" "$FILE_TO_FIND_USE_OF"|sed "s/Q/ Q/"|sed "s/$/\$/">work/tmp-patterns.list
	TOTAL_LINES=$(wc --lines <work/tmp-patterns.list)
	USESUM=0
	grep --max-count=1 -f work/tmp-patterns.list work/все_q_из_unknown_counts|sed "s/Q..*$//">work/tmp-counts
	while read qline ; do
		USESUM=$((USESUM + qline))
	done<work/tmp-counts
#	declare -ir COEFF=1000
	echo $((1000 * USESUM / TOTAL_LINES))
}

function ease_with_any_known() {
# для файла
# кол-во знакомых / кол-во незнакомых
# кол-во знакомых с учётом знакомости / кол-во незнакомых
	QFILE=$1
#	declare -i KNOWN_NUM
#	KNOWN_NUM=$(comm -12 <(sort "$QFILE") <(cat work/то,_что_знаю_из_чего-то_и_нужно_для_чего-то_ещё,_но_само_по_себе_не_изучал)|wc -l)
	
	KNOWN_NUM=$(comm -12 -- <(cut --delimiter="|" --fields=1 -- "$QFILE"|sort --) <(cat -- work/то,_что_знаю_из_чего-то_и_нужно_для_чего-то_ещё,_но_само_по_себе_не_изучал)|wc -l --)
#	echo "KNOWN_NUM: $KNOWN_NUM"
	declare -i QFILE_NUM
	QFILE_NUM=$(grep -c "^Q[0-9]" <"$QFILE")
#	echo "QFILE_NUM: $QFILE_NUM"
	declare -i UNKNOWN_NUM
	UNKNOWN_NUM=$((QFILE_NUM - KNOWN_NUM))
	# Avoid div by zero if there are no unknown items in the file:
	UNKNOWN_NUM=$((UNKNOWN_NUM + 1))
	declare -i EASE
	#EASE=$(( (KNOWN_NUM * 1000) / UNKNOWN_NUM ))
	# The "+ 2600" is to avoid negative result.  It's a bad idea, just
	# tuned to the files I currently have.
	EASE=$(( (KNOWN_NUM * 1000) / UNKNOWN_NUM - UNKNOWN_NUM + 3000 ))
	#EASE=$(( KNOWN_NUM - UNKNOWN_NUM ))
	echo $EASE
}



rm work/*
rm -rf work

REALWORK=$(mktemp --directory --tmpdir wdXXXXXXX)

ln -s $REALWORK ./work

mkdir -p work-save

if [ "$1" = "" ]
then
	declare -r UNK_FILES=unknown/*
else
	declare -r UNK_FILES=$@
fi

SKIP_IDS="25670|33057|36524|36578|54919|423048|620946"
SKIP_LIBS="193563|623578|1420342|1526131|2597810"
SKIP_LANGS="188|397|1860|7737"
#SKIP_SCIS="395|413|8087|8134|11190"
SKIP_ENCS="602358"
SKIP_WIKIMEDIA="151|263|565|964"
SKIP="Q(9135|19652|174376|$SKIP_IDS|$SKIP_LIBS|$SKIP_LANGS|$SKIP_ENCS|$SKIP_WIKIMEDIA)$"

cat -- known/*|grep -i "^Q[0-9]"|cut --delimiter="|" --fields=1|grep --color=never -v "^$"|egrep --color=never -v "$SKIP"|sort>work/все_q_из_known_без_дедупл
cat -- $UNK_FILES|grep -i "^Q[0-9]"|cut --delimiter="|" --fields=1|grep --color=never -v "^$"|egrep --color=never -v "$SKIP"|sort>work/все_q_из_unknown_без_дедупл
sort -- work/все_q_из_unknown_без_дедупл|uniq|cut --delimiter="|" --fields=1|fgrep --color=never -v --line-regexp ""|sort>work/все_q_из_unknown_уникально

cat -- work/все_q_из_known_без_дедупл work/все_q_из_unknown_уникально>work/все_q_из_known_без_дедупл_и_из_unknown_уникально

find -- known/*|cut --delimiter="/" --fields=2|cut --delimiter="@" --fields=1|sort >work/все_q_из_имён_в_known

find -- $UNK_FILES|grep --color=never -o "unknown/Q[0-9]\+@"|grep -o "Q[0-9]\+"|sort >work/все_q_из_имён_в_unknown
cat -- work/все_q_из_known_без_дедупл_и_из_unknown_уникально work/все_q_из_имён_в_unknown|sort>work/all_together_qs
uniq -c -w 15 -- work/all_together_qs>work/all_together_qs_counts
uniq -c -w 15 -d -- work/все_q_из_unknown_без_дедупл|sort -rn>work/все_q_из_unknown_counts

# Take the _top use_ or _top use possible_, and multiply all uses so that the top or top possible one is 1000.
#TOPNUM=$(find -- $UNK_FILES|wc -l)  # based on the maximum possible use
declare -i TOPNUM=$(egrep --max-count=1 -o "[0-9]+ " <work/все_q_из_unknown_counts)  # based on the actual maximum use
#normalize $TOPNUM work/все_q_из_unknown_counts >work/все_q_из_unknown_counts_normalized
cat -- work/все_q_из_known_без_дедупл work/все_q_из_имён_в_known|sort|uniq -c -w 15 >work/все_q_из_known_и_тамошних_имён_с_частотой

comm -12 -- <(uniq work/все_q_из_known_без_дедупл) <(cat work/все_q_из_unknown_уникально)>work/то_что_знаю_из_чегото_и_нужно_для_чегото_ещё
comm -13 -- <(comm -23 work/все_q_из_имён_в_known work/все_q_из_имён_в_unknown) <(sort -- work/то_что_знаю_из_чегото_и_нужно_для_чегото_ещё) >work/то,_что_знаю_из_чего-то_и_нужно_для_чего-то_ещё,_но_само_по_себе_не_изучал

sort work/all_together_qs|uniq>work/все_q_уникально

egrep --no-filename "^Q[0-9]+\|" -- known/*|sort -r|uniq>work/все_нормальные_строки_из_known
egrep --no-filename "^Q[0-9]+\|" -- $UNK_FILES|sort -r|uniq>work/все_нормальные_строки_из_unknown
#egrep --no-filename "^Q[0-9]+\|" unknown/*>work/все_нормальные_строки_из_unknown

egrep --no-filename "^Q[0-9]+\|" -- known/* $UNK_FILES|sort -r>work/все_нормальные_строки_из_known_и_unknown_без_дедупликации

function update_name_db() {

	uniq -c work/все_нормальные_строки_из_known_и_unknown_без_дедупликации|sort -rn>work/все_нормальные_строки_из_known_и_unknown_без_дедупликации_counts

	sed "s/^\s\+[0-9]\+\s\+//g" work/все_нормальные_строки_из_known_и_unknown_без_дедупликации_counts>work/все_нормальные_строки_из_known_и_unknown_без_дедупликации.sorted
	while read qline ; do

		grep --max-count=1 "^$qline|" work/все_нормальные_строки_из_known_и_unknown_без_дедупликации.sorted

	done<work/все_q_уникально >work/все_нормальные_строки_из_known_и_unknown_уникально
	cp work/все_нормальные_строки_из_known_и_unknown_уникально work-save/
}


if [ ! -e "work-save/все_нормальные_строки_из_known_и_unknown_уникально" ]
then
	update_name_db
fi


(echo all:
put_together all_together_qs_counts
echo known only:
put_together все_q_из_known_и_тамошних_имён_с_частотой
)|uniq


# берём список unknown
# для каждого файла находим ease, выводим ease и имя
# сортируем список по ease
# выводим после тестирования без ease
echo "По лёгкости:"
(for f in $UNK_FILES ; do
	EASE=$(ease_with_any_known "$f")
	echo "$EASE $f"
done)|sort -rn|sed "s=[^ ]\+unknown/==">work/ease_для_всех_файлов


declare -i TOPNUM=$(grep --max-count=1 -o "[0-9]\+ " <work/ease_для_всех_файлов)  # based on the actual maximum use
#normalize $TOPNUM work/ease_для_всех_файлов >work/ease_для_всех_файлов_normalized
#cat work/ease_для_всех_файлов_normalized
cat work/ease_для_всех_файлов

#usefulness_qs|put_use_together
grep -o "Q.*" work/все_q_из_unknown_counts>work/все_q_из_unknown
#grep -o "Q.*" work/все_q_из_unknown_counts_normalized>work/все_q_из_unknown


echo "Uses:"
put_uses_together>work/все_q_из_unknown_uses
head -n ${EASEANDUSE_BITABLE_HEIGHTS} work/все_q_из_unknown_uses

#echo "Use of a file:"
#use_of_file "unknown/Q388@Linux"|sort -rn

#echo "По полезности:"
#(for f in $UNK_FILES ; do
#	USE=$(use_of_file "$f")
#	echo "$USE $f"
#done)|sort -rn|sed "s=[^ ]\+unknown/=="

echo "По полезности и лёгкости:"
echo "Total Use Ease"
(for f in $UNK_FILES ; do
	# TODO: Write use+filename to a file, and then read from there.
	# TODO: Combine use of a file and use of the single Q the file represents.
	USE=$(use_of_file "$f")
	EASE=$(ease_with_any_known "$f")
#	echo "$USE × $EASE $f"
	#USEEASE=$(( (USE * 20) + EASE ))  # Болконский с use 12 и ease 2800 посередине, когда макс. use 40
	USEEASE=$(( (USE * 22) + EASE ))
	#USEEASE=$(( (USE * 40) + EASE ))
#	USEEASE=$(( (USE * 90) + EASE )) # Если use в %, то USE всегда перевешивает
	echo "${USEEASE} ${USE} ${EASE} $f"
done)|sort -rn|sed "s=[^ ]\+unknown/==">work/total_use_ease
head -n ${EASEANDUSE_BITABLE_HEIGHTS} work/total_use_ease

