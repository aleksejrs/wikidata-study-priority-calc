Put the directories from `example/` into the script’s root directory.

Imagine you know everything about cats: they have a head, a tail, and legs.
You know about dogs and giraffes only that they also have heads.
You put all that data into `known/` (files for each animal, listing the items
you know they have).

There is also to know (and you would like to know it, or it would have been in
`unneeded/`, which is not processed) that:
* Dogs also have legs and tails.
* Giraffes have legs, tails and necks.
* Camels are like giraffes, but they also have humps.
All of that is listed in `unknown/`.


# How it might work

As of this writing, here are some things the script considers, although it is
not exactly how it works or what it outputs.  It currently seems to me that
Bash supports only integer operations, so there are also some big
multiplications for division to work.

## Ease
Ease of a file is the familiar-to-unfamiliar ratio of the lines in it.
* You know everything about cats, so there is nothing to calculate for that
item.
* On dogs, you know one thing and don't know two.  That's probably not
considered right now.  What is, is that thanks to knowing about cats, you know
something about all parts of a dog.
* On giraffes, there is a complete unfamiliar part: a neck.  That makes the
giraffe harder to study than a dog.
* A camel has another unfamiliar part: a hump.  Yet harder to study.

## Usefulness of lines:

* All animals have heads, tails and legs.
* There are 3 animals of whose tails and legs you need to learn. So tails and
legs are the most useful lines to learn about first (usefulness: 3).
* The neck is a little less useful, because there are only two such animals
(usefulness: 2).
* You don't know about heads only concerning the camel, so “head” has the use
of 1 and is not considered (maybe that's a bug or only affects the output).
* The hump is the least useful thing to know, because learning about it only
helps you learn about camels (usefulness: 1), and it is harder to learn about
than the head.

## Usefulness of files:
The usefulness of a file in `unknown/` is based on the average usefulness of a
line in it.

## Usefulness + ease of a file:
Usefulness of a file is given a higher priority than its ease.

