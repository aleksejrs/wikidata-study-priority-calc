" Keep the first of two lines with the same Wikidata ID (prefer English etc)
%s/^\(Q\d\+\)|\(.*\)\n\1|.*$/\1|\2/g
sort | execute "%!uniq" | w | %s/^\(Q\d\+\)|\(.*\)\n\1|.*$/\1|\2/gc
" Keep the second of two lines with the same Wikidata ID (prefer Russian)
%s/^\(Q\d\+\)|\(.*\)\n\1|\(.*\)$/\1|\3/g

sort | execute "%!uniq" | w | %s/^\(Q\d\+\)|\(.*\)\n\1|\(.*\)$/\1|\3/gc

sort | execute "%!uniq" | w | %s/^\(Q\d\+\)|\(.*\)\n\1|\(.*\)$/\1|\2/gc | execute "%!sort -t '|' --key=2" | w 
